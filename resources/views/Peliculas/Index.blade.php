@extends('welcome')



@section('script_head')
<!-- DataTables CSS -->
<!-- Bootstrap Core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


@endsection



@section('content')




<!-- Informes de errores Request_Museo -->
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

@if (session('status'))
<div class="alert alert-success">
	{{ session('status') }}
</div>
@endif
@if (session('error'))
<div class="alert alert-danger">
	{{ session('error') }}
</div>
@endif

<div class="row">
	<div class="col-lg-12">

		<h1 class="page-header">Peliculas</h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">

		<div class="panel panel-primary">
			<div class="panel-heading">
				<!-- Trigger the modal with a button -->
				<button type="button" id='btn_Crear' class="btn btn-info btn-md" data-toggle="modal" data-target="#ModalCrear">Nuevo</button>
				<br><br>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th align="center">CODIGO</th>
							<th align="center">NOMBRE</th>							
							<th align="center">Género</th>
							<th align="center">Director</th>
							<th align="center">Año Lanzamiento</th>
							<th align="center">Imagen</th>
							<th align="center">Editar</th>
							<th align="center">Eliminar</th>
						</tr>
					</thead>
					<tbody>
						@foreach($ObjPelicula as $row )
						<tr  class="item{{ $row->codigo }}">
							<td align="center">{{ $row->codigo}}</td>
							<td align="center">{{ $row->nombre}}</td>
							<td align="center">{{ $row->genero}}</td>
							<td align="center">{{ $row->director}}</td>
							<td align="center">{{ $row->ano}}</td>
							<td align="center">
								<a href="{{trim($row->imagen)}}" target="_black"><button class="btn btn-info " ><i class="fa fa-picture-o" aria-hidden="true"></i></button></a>
							</td>
							<td align="center">     
								<button class="btn btn-success" onclick="Editar( {{$row->codigo}} )" >
									<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
								</button>    
							</td>
							<td align="center">         
								<button class="btn btn-danger" onclick="Eliminar({{$row->codigo}})" data-nombre="{{$row->genero}}"  >
									<i class="fa fa-trash-o" aria-hidden="true"></i>
								</button>
							</td>
						</tr>   
						@endforeach
					</tbody>
				</table>
				<!-- /.table-responsive -->
				<!-- Trigger the modal with a button -->		

			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- Fin modal -->	


<!-- Modal  -->
<div id="ModalEliminar" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<form action="{{ route('Eliminar') }}"  method="POST" name="Elimnar" id="Elimnar"   enctype="multipart/form-data" >
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					
				</div>
				<h4 class="modal-title">Eliminar Pelicula</h4>
				<input type="hidden" name="Codigo" id="Codigo1">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="modal-body">
					<p>Esta seguro de eliminar el registro?
					</p>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-danger ">Eliminar</button>
					<button type="button"   class="btn btn-warning" data-dismiss="modal">Cerrar</button>

				</div>

			</div>
		</form>

	</div>
</div>
<!-- Fin Modal  -->




<!-- Modal  -->
<div id="ModalCrear" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<form action="{{route('Nueva_Pelicula')}}" method="POST" name='Crear'  enctype="multipart/form-data" >

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

				</div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="modal-body">
					<h5 class="modal-title">Crear Pelicula</h5><br>


					<div class="col-md-12">
						Nombre Pelicula 

						<input type="text" name='Nombre' id='Nombre' required="required"  class="form-control" >
					</div>

					<div class="col-md-12">
						Genero 
						<select required="required" class="form-control" id="Genero"  name="Genero" tabindex="30">
							<option value="">Selecionar..</option>
							<option value="Ciencia Ficción">Ciencia Ficción</option>
							<option value="Terror">Terror </option>
							<option value="Comedia">Comedia </option>
							<option value="Acción">Acción </option>
						</select>	
					</div>

					<div class="col-md-12">
						Director

						<input type="text" name='Director' id='Director' required="required"  class="form-control" >
					</div>

					<div class="col-md-12">
						Año Lanzamiento 
							<select required="required" class="form-control" id="Año"  name="Año" tabindex="30">
							<option value="">Selecionar..</option>
							@for ($i = 1980; $i <= 2018; $i++) 
							<option value="{{ $i }}">{{$i}} </option>
							@endfor
						</select>	

					</div>


					<div class="col-md-12">
						Imagen 
						<br>
						<input type="file" name="Imagen" required="required" id='Imagen'>	
					</div>

				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary ">Guardar</button>
					<button type="button"   class="btn btn-danger"  data-dismiss="modal">Cerrar</button>

				</div>

			</div>
		</form>

	</div>
</div>





<!-- Modal Editar  -->
<div id="ModalEditar" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<form action="{{route('Editar')}}" method="POST" name='Crear'  enctype="multipart/form-data" >

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>

				</div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="modal-body">
					<h5 class="modal-title">Editar Pelicula</h5><br>

					<div class="col-md-12">
						Codigo Pelicula 

						<input type="text" readonly="true" name='Codigo' id='Codigo2'  class="form-control" >
					</div>
					<div class="col-md-12">
						Nombre Pelicula 

						<input type="text" name='Nombre' id='Nombre2' required="required"  class="form-control" >
					</div>

					<div class="col-md-12">
						Genero 
						<select required="required" class="form-control" id="Genero2"  name="Genero" tabindex="30">
							<option value="">Selecionar..</option>
							<option value="Ciencia Ficción">Ciencia Ficción</option>
							<option value="Terror">Terror </option>
							<option value="Comedia">Comedia </option>
							<option value="Acción">Acción </option>
						</select>	
					</div>

					<div class="col-md-12">
						Director

						<input type="text" name='Director' id='Director2' required="required"  class="form-control" >
					</div>

					<div class="col-md-12">
						Año Lanzamiento 
						<select required="required" class="form-control" id="Año"  name="Año2" tabindex="30">
							<option value="">Selecionar..</option>
							@for ($i = 1980; $i <= 2018; $i++) 
							<option value="{{ $i }}">{{$i}} </option>
							@endfor
						</select>		
					</div>


					<div class="col-md-12">
						Imagen 
						<br>
						<input type="file" name="Imagen" required="required" id='Imagen2'><br>	
						<div id='Imagen2'></div>
					</div>

				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary ">Guardar</button>
					<button type="button"   class="btn btn-danger"  data-dismiss="modal">Cerrar</button>

				</div>

			</div>
		</form>

	</div>
</div>





<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>

	function Eliminar(CODIGO) {
		$('#Codigo1').val(CODIGO);
		$('#ModalEliminar').modal('show');
	};


	function Editar(CODIGO) {

		$.ajax({
			type: 'post',
			url: 'Consulta',
			data: {
				'Codigo': CODIGO,
				'_token': $('input[name=_token]').val()
			},

			success: function(data) {

				for (var i = 0;i < data.length; i++) {

					$('#Codigo2').val(data[i].codigo);
					$('#Nombre2').val(data[i].nombre);
					$('#Director2').val(data[i].director);
					$('#Año2 > option[value="'+data[i].ano+'"]').attr('selected', 'selected');
					$('#Genero2 > option[value="'+data[i].genero+'"]').attr('selected', 'selected');
				
		
					obj='<a href="'+data[i].imagen+'" target="_blank" ><i style="color: red"> Imagen</i></a>'
					$('#Imagen2').append(obj);

				}		
			}
		});


		$('#ModalEditar').modal('show');
	}

// Solo permite ingresar numeros.
function soloNumeros(e){
	var key = window.Event ? e.which : e.keyCode
	return (key >= 48 && key <= 57)
}


</script>
@endsection



