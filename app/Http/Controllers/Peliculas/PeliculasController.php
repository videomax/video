<?php namespace App\Http\Controllers\Peliculas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\peliculas;

class PeliculasController extends Controller
{


//Consulta toda la tabla ----------------------------------------------------------------
	public function Vista(){


		$ObjPelicula = \DB::select(" select * from peliculas order by 1 ");

		return view('Peliculas.Index',compact('ObjPelicula'));
	}


	public function Crear_Pelicula(Request $request){


		$sError=null;

		try{


			$file= $request->file('Imagen');

			$tabla = new peliculas;
			$tabla->nombre=$request->Nombre;
			$tabla->genero=$request->Genero;
			$tabla->director=$request->Director;
			$tabla->ano=$request->Año;
			$tabla->imagen="Imagenes/".$request->Nombre."/".$file->getClientOriginalName();
			$tabla->save();

			$file->move("Imagenes/".$request->Nombre,$file->getClientOriginalName());



		}catch(\Exception $e){

			dd($e);
			$sError= $e->getMessage();
		}

		if($sError==null){

			$request->session()->flash('status', 'Se creo la pelicula  Exitosamente!!');
		}else{
			$request->session()->flash('error', '!! Error al Crear la pelicula. !! '.$sError);
		}

		return redirect('Peliculas');


	}

	public function Eliminar_Pelicula(Request $request)
	{

		$sError=null;

		try{

			peliculas::destroy($request->Codigo);

		}catch(\Exception $e){

			$sError= $e->getMessage();
		}

		if($sError==null){

			$request->session()->flash('status', 'Se Elimino la pelicula Nro. '.$request->Codigo.'  Exitosamente!!');
		}else{
			$request->session()->flash('error', '!! Error al Elimino la pelicula. !! '.$sError);
		}

		return redirect('Peliculas');

	}


	public function Editar_Pelicula(request $request)
	{

		$sError=null;

		try{

			
			$file= $request->file('Imagen');

			$tabla = peliculas::find($request->Codigo);
			$tabla->nombre=$request->Nombre;
			$tabla->genero=$request->Genero;
			$tabla->director=$request->Director;
			$tabla->ano=$request->Año;
			$tabla->imagen="Imagenes/".$request->Nombre."/".$file->getClientOriginalName();
			$tabla->save();

			$file->move("Imagenes/".$request->Nombre,$file->getClientOriginalName());

		}catch(\Exception $e){

			$sError= $e->getMessage();
		}

		if($sError==null){

			$request->session()->flash('status', 'Se Elimino la pelicula Nro. '.$request->Codigo.'  Exitosamente!!');
		}else{
			$request->session()->flash('error', '!! Error al Elimino la pelicula. !! '.$sError);
		}
		return redirect('Peliculas');


	}
	public function Consulta_Pelicula(request $request){

		
		$ObjPelicula = \DB::select(" select * from peliculas where codigo='".$request->Codigo."'");

		return response()->json($ObjPelicula);

	}

}
