<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class peliculas extends Model
{

	//Nombre de la tabla
	protected $table = 'peliculas';

	//lleve primaria
    protected $primaryKey = 'codigo';

    // Parametro para no actualizar fechas de actualizacion
    public $timestamps = false;


}
