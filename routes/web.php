<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('Home', function () {
    return view('home');
});


// Peliculas
Route::get('Peliculas', 'Peliculas\PeliculasController@Vista');

Route::post('Nueva_Pelicula',['as'=>'Nueva_Pelicula', 'uses'=>'Peliculas\PeliculasController@Crear_Pelicula']);

Route::post('Eliminar',['as'=>'Eliminar', 'uses'=>'Peliculas\PeliculasController@Eliminar_Pelicula']);

Route::post('Consulta',['as'=>'Consulta', 'uses'=>'Peliculas\PeliculasController@Consulta_Pelicula']);

Route::post('Editar',['as'=>'Editar', 'uses'=>'Peliculas\PeliculasController@Editar_Pelicula']);